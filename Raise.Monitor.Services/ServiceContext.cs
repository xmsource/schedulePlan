﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Raise.Monitor.Model;
using Raise.Monitor.Tools;

namespace Raise.Monitor.Services {
    public class ServiceContext {
        public static Strategy Execute() {
            var connection = ConfigurationManager.ConnectionStrings["Setting|Monitor"];
            if(connection == null || string.IsNullOrEmpty(connection.ProviderName))
                throw new Exception("请检查数据库配置以及ProviderName的值");
            switch(connection.ProviderName.ToUpper()) {
                case "MSSQL":
                    return new MySqlStrategy();
                case "MYSQL":
                    return new MySqlStrategy();
                case "ORACLE":
                    return new OracleStrategy();
                default:
                    throw new Exception("ProviderName值错误");
            }
        }
    }
}
